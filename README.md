# Firefly files for fortune

This repository contains files for quotes from the series "Firefly" for
the fortune program.

Copy the files `firefly` and `firefly.dat` into the appropriate folder
of your fortune installation to make them usable.

